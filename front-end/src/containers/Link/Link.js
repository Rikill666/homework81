import React, {Component} from 'react';
import {Button, Form, FormGroup, Input} from "reactstrap";
import {connect} from "react-redux";
import {addLink, getLinkByShortUrl} from "../../store/actions/linkAction";
import Spinner from "../../components/UI/Spinner/Spinner";

class Link extends Component {
    state ={
        originalUrl:"",
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    submitFormHandler = async(event) => {
        event.preventDefault();
        await this.props.addLink({...this.state});

    };

    render() {
        return (
            <div>
                <h2>Shorten your link</h2>
                <Form onSubmit={this.submitFormHandler} style={{marginBottom:"30px"}}>
                    <FormGroup>
                        <Input type="text"
                               name="originalUrl"
                               id="originalUrl"
                               placeholder="Enter URL here"
                               value={this.state.originalUrl}
                               onChange={this.inputChangeHandler}
                        />
                    </FormGroup>
                    <Button>Shorten!</Button>
                </Form>
                {this.props.shortUrl?this.props.loading?<Spinner/>:
                <div>
                    <h4>Your link now looks like this: </h4>
                    <a href={"http://localhost:8000/" + this.props.shortUrl}>{"http://localhost:8000/" + this.props.shortUrl}</a>
                </div>: null}
            </div>
        );
    }
}
const mapStateToProps = state => ({
    shortUrl:state.link.shortUrl,
    loading: state.link.loading,
    error: state.link.error
});

const mapDispatchToProps = dispatch => ({
    addLink: (link) => dispatch(addLink(link)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Link);