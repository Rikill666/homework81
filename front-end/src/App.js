import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import Link from "./containers/Link/Link";


const App = () => {
  return (
      <Container style={{textAlign:"center"}}>
        <Switch>
          <Route path="/" exact component={Link}/>
          <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
      </Container>
  );
};

export default App;