import {
    CREATE_LINK_ERROR, CREATE_LINK_REQUEST, CREATE_LINK_SUCCESS,
} from "../actions/linkAction";

const initialState = {
    shortUrl: "",
    loading: false,
    error: null,
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_LINK_REQUEST:
            return {...state, loading: true};
        case CREATE_LINK_SUCCESS:
            return {...state, shortUrl: action.shortUrl, error:null, loading: false};
        case CREATE_LINK_ERROR:
            return {...state,error:action.error, loading: false};
        default:
            return state;
    }
};

export default newsReducer;