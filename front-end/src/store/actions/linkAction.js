import axiosApi from "../../axiosApi";

export const CREATE_LINK_REQUEST = 'CREATE_LINK_REQUEST';
export const CREATE_LINK_SUCCESS = 'CREATE_LINK_SUCCESS';
export const CREATE_LINK_ERROR= 'CREATE_LINK_ERROR';


export const createLinkRequest = () => {return {type: CREATE_LINK_REQUEST};};
export const createLinkSuccess = shortUrl => ({type: CREATE_LINK_SUCCESS, shortUrl});
export const createLinkError = (error) => {return {type: CREATE_LINK_ERROR, error};};

export const addLink = (link) => {
    return async dispatch => {
        try{
            dispatch(createLinkRequest());
            const response = await axiosApi.post('/links', link);
            const shortUrl = response.data.shortUrl;
            dispatch(createLinkSuccess(shortUrl));
        }
        catch (e) {
            dispatch(createLinkError(e));
        }
    };
};
