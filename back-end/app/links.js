const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');
const router = express.Router();

router.get("/", async (req, res) => {
    const links = await Link.find();
    res.send(links);
});



const shortUrlGenerations = async() => {
    const shortUrl = nanoid(6);
    const links = await Link.find({"shortUrl": shortUrl});
    if(links[0]){
        await shortUrlGenerations();
    }
    else{
        return shortUrl;
    }
};

router.post('/', async (req, res) => {
    const shortUrl = await shortUrlGenerations();
    const linkData = {...req.body, shortUrl: shortUrl};
    try {
        const link = new Link(linkData);
        await link.save();
        res.send({shortUrl: link.shortUrl});
    } catch (e) {
        return res.status(400).send(e);
    }
});


module.exports = router;