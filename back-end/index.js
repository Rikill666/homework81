const express = require('express');
const cors = require('cors');
const links= require('./app/links');
const mongoose = require("mongoose");


const app = express();
app.use(cors());

const port = 8000;

app.use(express.json());
app.use(express.static('public'));

const Link = require('./models/Link');

app.get("/:shortUrl", async (req, res) => {
    try{
        const shortUrl = req.params.shortUrl;
        const link = await Link.findOne({"shortUrl": shortUrl});
        if (!link) {
            return res.status(404).send({message: "Not found"});
        }
        res.redirect(301, link.originalUrl);
    }
    catch (e) {
        res.status(404).send({ message: "Not found" });
    }
});

const run = async () =>{
    await mongoose.connect('mongodb://localhost/linkChanger', {
        useNewUrlParse: true,
        useUnifiedTopology: true
    });
    app.use('/links', links);
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

};

run().catch(e=>{
    console.log(e);
});


