const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const linkScheme = new Scheme({
    originalUrl:{
        type: String,
        required:true
    },
    shortUrl:{
        type: String,
        required:true
    }
});

const Link = mongoose.model("Link", linkScheme);
module.exports = Link;